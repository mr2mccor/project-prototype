import threading, sys
from Tkinter import *
from bottle import run, route, static_file, template


#WIZARD OF OZ CODE HERE

gui = Tk()

gui.geometry('400x400')
gui.title('Wizard of Oz Tools')

walletchecked = IntVar()
walletcheck = Checkbutton(gui, text="Wallet In Place", variable=walletchecked)
walletcheck.pack()

keyschecked = IntVar()
keyscheck = Checkbutton(gui, text="Keys In Place", variable=keyschecked)
keyscheck.pack()

medicinechecked = IntVar()
medicinecheck = Checkbutton(gui, text="Medicine In Place", variable=medicinechecked)
medicinecheck.pack()

bookchecked = IntVar()
bookcheck = Checkbutton(gui, text="Book In Place", variable=bookchecked)
bookcheck.pack()

slider_label = Label(text='Distance Slider: ')
slider_label.pack()

slider = Scale(gui, from_=0, to=100, orient=HORIZONTAL)
slider.pack()

#def on_closing():
#	print("Go back to other thread somehow!")
#	server_thread.sendcontrol('c')
#	gui.quit()

#gui.protocol("WM_DELETE_WINDOW", on_closing)




#WEB SERVER CODE HERE

@route('/debug')
def debug_page():
	global slider, walletchecked, keyschecked, medicinechecked, bookchecked
	return "{ \"percent\":" + str(slider.get()) + ", \"wallet\":" + str(walletchecked.get()) + ", \"keys\":" + str(keyschecked.get()) + ", \"medicine\":" + str(medicinechecked.get()) + ", \"book\":" + str(bookchecked.get()) + "}"

@route('/')
def main_page():
	return static_file('login.html', root='./')

@route('/find-tag/<tag_name>')
def find_tag_page(tag_name):
	return "Find tag " + tag_name

@route('/bower_components/<path>/<filename>')
def static_image(path, filename):
	return static_file(filename, root='./bower_components/' + path + '/')

@route('/<filename>')
def static_image(filename):
	return static_file(filename, root='./')


server_thread = threading.Thread(target=run, kwargs=dict(host='0.0.0.0', port=8000, debug=True))
server_thread.start()


gui.mainloop()