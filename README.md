# Project Prototype II #

The prototype website was made with HTML, Javascript, and Google Polymer for the UI elements. The backend server was made with Python and the modules Bottle and TKinter.

### How to Run ###

- Install Python pip with the command "sudo apt-get install python-pip" on Linux
- Install Bottle module for Python with command "pip install bottle" or "sudo pip install bottle" for Linux
- Install TKinter module for Python with the command "sudo apt-get install python-tk" for Linux
- Run server with command "python server.py" or "python2 server.py" if Python3 is installed
- Make sure to use Python 2
- Go to address "localhost:8000" or "<Server IP>:8000" on a phone
- It is designed to work best on Chrome for phones
- To exit close the terminal
- For beeping while locating to work go to "chrome://flags" and enable the "Disable gesture requirement for media playback"
- Tested and working with Chrome on Android and Chrome on PC
- Doesn't work with Firefox or on Iphone
